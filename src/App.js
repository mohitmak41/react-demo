import logo from './logo.svg';
import './App.css';

import React from 'react';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';


function App() {
  return (
    <div className="App">       
    <header class="header">  
        <div class="stripe">   
            <div class="container">
                <div class="row">
                    <div class="col-md-7 left">
                        <ul>
                            <li><a href="#">भारत सरकार</a></li>
                            <li><a href="#">Government of India</a></li>
                            <li><a href="#">Sitemap</a></li>
                            <li><a href="#"><i class="fas fa-phone-alt"></i> Helpdesk No. 14434/Monday To Saturday</a></li>                        
                        </ul>
                    </div>
                    <div class="col-md-5 right">
                        <ul>
                            <li><a href="#">Screen Reader Access</a></li>
                            <li><a href="#">Skip To Main Content</a></li>
                            <li>
                                <div class="incrs">
                                    <a href="#">A-</a>
                                    <a href="#">A</a>
                                    <a href="#">A+</a>
                                </div>
                            </li>
                            <li><div class="radio">
                                    <input id="male" class="checkbox" name="check" type="radio" value=""/>
                                    <label for="male"> <span class="check"></span></label>
                                    <input id="female" class="checkbox" name="check" type="radio" value="" checked="checked"/>
                                    <label for="female"><span class="check"></span></label>
                                </div></li>
                            <li><a href="#">English</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="mid-side">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 site-logo">
                        <a href="#"><img src="http://134.209.222.136/mle/images/logo.png"/></a>
                    </div>
                    <div class="col-md-6 top-logo">
                        <ul>
                            <li><a href="#"><img src="http://134.209.222.136/mle/images/e-shram.png"/></a></li>
                            <li><a href="#"><img src="http://134.209.222.136/mle/images/g20.png"/></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="mobile_menu">
                <div class="fa-bars">
                    <i class="fas fa-bars"></i>
                </div>                        
            </div>
        </div>
        <div class="menu-side">
            <div class="container">
                <nav>
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li class="sub_menu"><a href="#">About Us</a>
                            <div class="drop_box">
                                <ul>
                                    <li><a href="#">Add Menu</a></li>
                                    <li><a href="#">Add Menu</a></li>
                                    <li><a href="#">Add Menu</a></li>
                                    <li><a href="#">Add Menu</a></li>                                                                        
                                </ul>
                            </div>
                        </li>
                        <li class="sub_menu"><a href="#">Schemes</a>
                            <div class="drop_box">
                                <ul>
                                    <li><a href="#">Add Menu</a></li>
                                    <li><a href="#">Add Menu</a></li>
                                    <li><a href="#">Add Menu</a></li>
                                    <li><a href="#">Add Menu</a></li>                                                                        
                                </ul>
                            </div>
                        </li>
                        <li><a href="#">Acts & Rules</a></li>
                        <li class="sub_menu"><a href="#">Media</a>
                            <div class="drop_box">
                                <ul>
                                    <li><a href="#">Add Menu</a></li>
                                    <li><a href="#">Add Menu</a></li>
                                    <li><a href="#">Add Menu</a></li>
                                    <li><a href="#">Add Menu</a></li>                                                                        
                                </ul>
                            </div>
                        </li>
                        <li><a href="#">Stakeholder</a></li>
                        <li class="sub_menu"><a href="#">Service</a>
                            <div class="drop_box">
                                <ul>
                                    <li><a href="#">Add Menu</a></li>
                                    <li><a href="#">Add Menu</a></li>
                                    <li><a href="#">Add Menu</a></li>
                                    <li><a href="#">Add Menu</a></li>                                                                        
                                </ul>
                            </div>
                        </li>
                        <li><a href="#">Dashboard</a></li>
                        <li class="sub_menu"><a href="#">Contact Us</a>
                            <div class="drop_box">
                                <ul>
                                    <li><a href="#">Add Menu</a></li>
                                    <li><a href="#">Add Menu</a></li>
                                    <li><a href="#">Add Menu</a></li>
                                    <li><a href="#">Add Menu</a></li>                                                                        
                                </ul>
                            </div>
                        </li>
                        <li><a href="#">CSC Locator</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="left_menu">
        <div class="fa_times">
            <i class="fas fa-times"></i>
        </div>
        
    </div>
    </header>
        
    <section class="banner_section">
        <div class="overlay">
            <div class="banner-slider">
                <OwlCarousel className='banner_slider' items={1} margin={0} autoplay={true} dots={false}>
                    <div class='item'>
                    <img src="http://134.209.222.136/mle/images/banner-01.png"/>
                    </div>
                    <div class='item'>
                    <img src="http://134.209.222.136/mle/images/banner-01.png"/>
                    </div>                    
                </OwlCarousel>                
            </div>            
        </div>
        <div class="banner_logo_section">
        <div class="container">
                <div class="banner_logo">
                    <OwlCarousel className='banner-logo-slider' items={5} margin={0} autoplay={true} dots={false}>
                        <div class="item">
                            <a href="#"><img src="http://134.209.222.136/mle/images/banner-logo-01.png"/> <span>myScheme</span></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="http://134.209.222.136/mle/images/banner-logo-02.png"/> <span>Skill India</span></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="http://134.209.222.136/mle/images/banner-logo-03.png"/> <span>Apprenticeship</span></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="http://134.209.222.136/mle/images/banner-logo-04.png"/> <span>Job</span></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="http://134.209.222.136/mle/images/banner-logo-05.png"/> <span>Pension</span></a>
                        </div>
                        <div class="item">
                            <a href="#"><img src="http://134.209.222.136/mle/images/banner-logo-06.png"/> <span>Digital Skills Training</span></a>
                        </div>
                    </OwlCarousel>                    
                </div>
            </div>
        </div>
    </section>   

    <section class="marquee-section">
        <div class="container">
        <div class="row">
            <div class="col-md-2 left">                
                <h3>WhatsNew</h3>                
            </div>
            <div class="col-md-9 mid">
                <marquee id="marq" direction="left" onmouseover="this.stop();" onmouseout="this.start();" scrollamount="5">
                <ul>
                    <li>Launch of new features in eShram portal by Shri Bhupender Yadav, Hon’ble Minister for Labour & Employment and Environment, Forest and Climate Change.</li>
                    <li>Launch of new features in eShram portal by Shri Bhupender Yadav, Hon’ble Minister for Labour & Employment and Environment, Forest and Climate Change.</li>
                </ul>                
                </marquee>
            </div>
            <div class="col-md-1 right">                                                
                    <button>
                    <div class="playpause">
                    <input onclick="toggleMarquee();" type="checkbox" value="None" id="playpause" name="check" />
                    <label for="playpause"></label>
                    </div>  
                    </button>              
                
            </div>
        </div>
        </div>
    </section>

    
    <section class="about_section">
        <div class="container">
            <div class="row center">
                <div class="col-md-4 about">
                    <div class="text">
                        <h2>About Ministry</h2>
                        <p>The Ministry of Labour & Employment is one of the oldest and important Ministries of the Government of India. The main responsibility of the Ministry is to protect and safeguard the interests of workers in general and those who constitute the poor, deprived and disadvantage sections of the society, in particular, with due regard to creating a healthy work environment for higher production and productivity and to develop and coordinate vocational skill training and employment services. Government’s attention is also focused on promotion of welfare and providing social security to the labour force both in organized and unorganized sectors, in tandem with the process of liberalization.</p>
                        <a href="#" class="bg-btn">Read more</a>
                    </div>
                </div>
                <div class="col-md-4 cards">
                    <div class="text">
                        <ul>
                            <li>
                                <div class="row">
                                    <div class="col-md-4 left">
                                        <img src="http://134.209.222.136/mle/images/id-card.png"/>
                                    </div>
                                    <div class="col-md-8 right">
                                        <span>eShram Cards Issued</span>
                                        <h3>28,96,88,553</h3>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-md-4 left">
                                        <img src="http://134.209.222.136/mle/images/register.png"/>
                                    </div>
                                    <div class="col-md-8 right">
                                        <span>For Pension of Rs.3000/ Month</span>
                                        <h3>Register on maandhan.in</h3>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-md-4 left">
                                        <img src="http://134.209.222.136/mle/images/membership.png"/>
                                    </div>
                                    <div class="col-md-8 right">
                                        <span>Registration is totally free</span>
                                        <h3>REGISTER on eShram</h3>
                                        <em>Already Registered? <b>Update</b></em>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 leader">
                    <div class="text">
                        <ul>
                            <li>
                                <div class="row">
                                    <div class="col-md-4 left">
                                        <img src="http://134.209.222.136/mle/images/leader-01.png"/>
                                    </div>
                                    <div class="col-md-8 right">
                                        <h3>Shri Narendra Modi</h3>
                                        <span>Prime Minister of India</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-md-4 left">
                                        <img src="http://134.209.222.136/mle/images/leader-02.png"/>
                                    </div>
                                    <div class="col-md-8 right">
                                        <h3>Shri Bhupender Yadav</h3>
                                        <span>Hon'ble Minister</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="row">
                                    <div class="col-md-4 left">
                                        <img src="http://134.209.222.136/mle/images/leader-03.png"/>
                                    </div>
                                    <div class="col-md-8 right">
                                        <h3>Shri Rameswar Teli</h3>
                                        <span>Hon'ble MoS</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    

    
    <section class="feature_section">
        <div class="overlay">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 same-box">
                        <div class="inner inner1">
                            <div class="row">
                                <div class="col-md-5 left">
                                    <div class="image">
                                        <img src="http://134.209.222.136/mle/images/diagram.png"/>
                                    </div>
                                </div>
                                <div class="col-md-5 right">
                                    <div class="text">
                                        <h3>Schemes</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 same-box">
                        <div class="inner">
                            <div class="row">
                                <div class="col-md-5 left">
                                    <div class="image">
                                        <img src="http://134.209.222.136/mle/images/networking.png"/>
                                    </div>
                                </div>
                                <div class="col-md-5 right">
                                    <div class="text">
                                        <h3>Stakeholder</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 same-box">
                        <div class="inner inner3">
                            <div class="row">
                                <div class="col-md-5 left">
                                    <div class="image">
                                        <img src="http://134.209.222.136/mle/images/technical-support.png"/>
                                    </div>
                                </div>
                                <div class="col-md-5 right">
                                    <div class="text">
                                        <h3>Services</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    

    
    <section class="event_section">
        <div class="container">
            <div class="row">                
                <div class="col-md-8 news-side">
                    <h2>Upcoming News & Events</h2>
                    <div class="row">
                        <div class="col-md-6 left">
                            <div class="content">
                                <a href="#">
                                    <img src="http://134.209.222.136/mle/images/event-01.jpg"/>
                                </a>
                                <div class="text">
                                    <h3>Events- 1</h3>
                                    <p>The Ministry of Labour & Employment is one of the oldest and important Ministries of the Government of India. The main responsibility of the Ministry is to protect and safeguard the interests of workers..</p>
                                    <a href="#" class="bg-btn">View All</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 right">
                            <div class="content">
                                <ul>
                                    <li><a href="#">
                                        <div class="row">
                                            <div class="col-md-5 image">                                                
                                                <img src="http://134.209.222.136/mle/images/event-02.jpg"/>                                                
                                            </div>
                                            <div class="col-md-7 text-inner">
                                                <div class="text">
                                                    <h3>Events- 2</h3>
                                                    <p>The Ministry of Labour & Employment is one of the oldest and</p>
                                                </div>
                                            </div>
                                        </div></a>
                                    </li>
                                    <li><a href="#">
                                        <div class="row">
                                            <div class="col-md-5 image">                                                
                                                <img src="http://134.209.222.136/mle/images/event-03.jpg"/>                                                
                                            </div>
                                            <div class="col-md-7 text-inner">
                                                <div class="text">
                                                    <h3>Events- 3</h3>
                                                    <p>The Ministry of Labour & Employment is one of the oldest and</p>
                                                </div>
                                            </div>
                                        </div></a>
                                    </li>
                                    <li><a href="#">
                                        <div class="row">
                                            <div class="col-md-5 image">                                                
                                                <img src="http://134.209.222.136/mle/images/event-04.jpg"/>                                                
                                            </div>
                                            <div class="col-md-7 text-inner">
                                                <div class="text">
                                                    <h3>Events- 4</h3>
                                                    <p>The Ministry of Labour & Employment is one of the oldest and</p>
                                                </div>
                                            </div>
                                        </div></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 testimonial">
                    <div class="inner-right">
                        <div class="overlay">
                    <h2>Testimonials</h2>           
                    <OwlCarousel className='testimonial-slider' items={1} margin={0} autoplay={true} dots={false}>
                        <div class="item">
                            <div class="slide-box">
                                <div class="image">
                                    <img src="http://134.209.222.136/mle/images/testimonials-01.svg"/>
                                </div>
                                <h4>Testimonials</h4>
                                <p>Testimonials from various unorganised workers who got benefitted by registering to this portal and allied schemes.</p>
                                <a href="#">View all</a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="slide-box">
                                <div class="image">
                                    <img src="http://134.209.222.136/mle/images/testimonials-01.svg"/>
                                </div>
                                <h4>Testimonials</h4>
                                <p>Testimonials from various unorganised workers who got benefitted by registering to this portal and allied schemes.</p>
                                <a href="#">View all</a>
                            </div>
                        </div>
                    </OwlCarousel>                             
                    </div>                
                    </div>                    
                </div>
            </div>
        </div>
    </section>
    

    
    <section class="gallery_section">
        <div class="container">
            <div class="row">
                <div class="col-md-8 left">
                    <h2>Photo Gallery</h2>                    
                    <span><a href="#">View all</a></span>
                    <div class="tz-gallery">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="image">
                                    <a class="lightbox" href="http://134.209.222.136/mle/images/gallery-01.jpg">
                                        <img src="http://134.209.222.136/mle/images/gallery-01.jpg"/>
                                    </a>                                    
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="image top_image">
                                    <a class="lightbox" href="http://134.209.222.136/mle/images/gallery-02.jpg">
                                        <img src="http://134.209.222.136/mle/images/gallery-02.jpg"/>
                                    </a>                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-6 same">
                                        <div class="image">
                                            <a class="lightbox" href="http://134.209.222.136/mle/images/gallery-03.jpg">
                                                <img src="http://134.209.222.136/mle/images/gallery-03.jpg"/>
                                            </a>                                    
                                        </div>
                                    </div>
                                    <div class="col-md-6 same">
                                        <div class="image">
                                            <a class="lightbox" href="http://134.209.222.136/mle/images/gallery-04.jpg">
                                                <img src="http://134.209.222.136/mle/images/gallery-04.jpg"/>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 right">
                    <h2>Video Gallery</h2>
                    <span><a href="#">View all</a></span>
                    <div class="image">
                        <img src="http://134.209.222.136/mle/images/video.jpg"/>
                        <div class="overlay">
                        <div class="play_pause">
                            <div class="waves wave-1"></div>
                            <div class="waves wave-2"></div>
                            <div class="waves wave-3"></div>                           
                        </div>
                        <a href="#" data-toggle="modal" data-target="#exampleModalCenter"><i class="fas fa-play"></i></a>                   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    

    
    <section class="logo_section">
        <div class="container">
            <OwlCarousel className='logo-slider' items={5} margin={0} autoplay={true} dots={false}>
                <div class="item">
                    <div class="image">
                        <img src="http://134.209.222.136/mle/images/logo-01.png"/>
                    </div>
                </div>
                <div class="item">
                    <div class="image">
                        <img src="http://134.209.222.136/mle/images/logo-02.png"/>
                    </div>
                </div>
                <div class="item">
                    <div class="image">
                        <img src="http://134.209.222.136/mle/images/logo-03.png"/>
                    </div>
                </div>
                <div class="item">
                    <div class="image">
                        <img src="http://134.209.222.136/mle/images/logo-04.png"/>
                    </div>
                </div>
                <div class="item">
                    <div class="image">
                        <img src="http://134.209.222.136/mle/images/logo-05.png"/>
                    </div>
                </div>
                <div class="item">
                    <div class="image">
                        <img src="http://134.209.222.136/mle/images/logo-06.png"/>
                    </div>
                </div>
            </OwlCarousel>               
        </div>
    </section>
    

    
    <footer>
        <div class="container">        
            <div class="row">
                <div class="col-md-9 left">
                    <ul>
                        <li><a href="#">Accessibility Statement</a></li>
                        <li><a href="#">Terms & Conditions</a></li>
                        <li><a href="#">Disclaimer</a></li>
                        <li><a href="#">Website Policies</a></li>
                        <li><a href="#">Help</a></li>
                        <li><a href="#">Contact Us</a></li>
                        <li><a href="#">Web Information Manager</a></li>
                        <li><a href="#">Glossary</a></li>
                        <li><a href="#">FAQs</a></li>
                        <li><a href="#">Sitemap</a></li>
                    </ul>
                    <p>©2023 Ministry Of Labour & Employment. All rights reserved.</p>
                </div>
                <div class="col-md-3 right">
                    <span>Visitors : 117080</span>
                    <span>Last Update: 20-Jul-2023</span>
                </div>
            </div>
        </div>
    </footer>
        
    <div class="social_sidebar">
      <div class="social facebook">
        <a href="#" target="_blank">
          <i class="fab fa-facebook-f"></i>
        </a>
      </div>
      <div class="social twitter">
        <a href="#" target="_blank">
          <i class="fab fa-twitter"></i>
        </a>
      </div>
      <div class="social youtube">
        <a href="#" target="_blank">
          <i class="fab fa-youtube"></i>
        </a>
      </div>
      <div class="social instagram">
        <a href="#" target="_blank">
          <i class="fab fa-instagram"></i>
        </a>
      </div>
    </div>
    
    </div>
  );
}

export default App;
